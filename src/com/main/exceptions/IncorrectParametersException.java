package com.main.exceptions;

import com.main.DateCalculator;

/**
 * This exception is raised when arguments required for running {@link DateCalculator} are not provided.
 */
public class IncorrectParametersException extends RuntimeException {
    public IncorrectParametersException() {
        super("Incorrect parameters, required: YYYY-MM-DD YYYY-MM-DD");
    }
}
