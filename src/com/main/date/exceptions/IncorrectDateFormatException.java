package com.main.date.exceptions;

/**
 * This exception is raised when requested to convert value with incorrect format.
 */
public class IncorrectDateFormatException extends RuntimeException {
    public IncorrectDateFormatException(String date) {
        super(String.format("Incorrect date %s (required format is YYYY-MM-DD).", date));
    }
}
