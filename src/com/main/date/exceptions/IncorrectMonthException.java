package com.main.date.exceptions;

import com.main.date.Date;

/**
 * This exception is raised when requested to instantiate {@link Date}
 * with incorrect month.
 */
public class IncorrectMonthException extends RuntimeException {
    public IncorrectMonthException(int month) {
        super(String.format("Incorrect month %d (value not in range 1-12).", month));
    }
}
