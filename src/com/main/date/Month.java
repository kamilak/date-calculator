package com.main.date;

/**
 * Enum representing the 12 months of the year.
 */
public enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

    /**
     * @return numeric representation of {@code Month}.
     */
    public int number() {
        return ordinal() + 1;
    }
}
