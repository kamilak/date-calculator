package com.main.date.validations;

import com.main.date.Date;
import com.main.date.exceptions.IncorrectDayException;
import com.main.date.exceptions.IncorrectMonthException;
import com.main.date.exceptions.IncorrectYearException;
import com.main.date.utils.DateUtils;

import static com.main.date.Month.DECEMBER;
import static com.main.date.Month.JANUARY;

/**
 * Class for validating {@link Date} parameters.
 */
public class DateValidator {

    private int year;
    private int month;
    private int day;

    public DateValidator(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public boolean validate() {
        return validateYear() &&  validateMonth() && validateDayOfMonthInYear();
    }

    private boolean validateYear() {
        if (year < DateUtils.YEAR_MIN || year > DateUtils.YEAR_MAX) {
            throw new IncorrectYearException(year);
        }
        return true;
    }

    private boolean validateMonth() {
        if (month < JANUARY.number() || month > DECEMBER.number()) {
            throw new IncorrectMonthException(month);
        }
        return true;
    }

    private boolean validateDayOfMonthInYear() {
        int endDayOfMonth = DateUtils.getEndDayOfMonth(this.month, year);
        if (day < 1 || day > endDayOfMonth) {
            throw new IncorrectDayException(day, endDayOfMonth);
        }
        return true;
    }
}
