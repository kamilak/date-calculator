package com.main.date.exceptions;

import com.main.date.Date;
import static com.main.date.utils.DateUtils.*;

/**
 * This exception is raised when requested to instantiate {@link Date}
 * with the year outside range specified in {@link com.main.date.utils.DateUtils}.
 */
public class IncorrectYearException extends RuntimeException{
    public IncorrectYearException(int year) {
        super(String.format("Incorrect year %d (value not in range: %d-%d).", year, YEAR_MIN, YEAR_MAX));
    }
}
