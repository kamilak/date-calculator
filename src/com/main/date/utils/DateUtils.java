package com.main.date.utils;

import static com.main.date.Month.DECEMBER;
import static com.main.date.Month.FEBRUARY;
import static com.main.date.Month.JANUARY;

public class DateUtils {
    public static final int YEAR_MIN = 1901;
    public static final int YEAR_MAX = 2999;

    private final static int[] DAYS_IN_MONTH = new int[] { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public static int getEndDayOfMonth(int month, int year) {
        if (isLeap(year) && month == FEBRUARY.number()) { return 29; }
        return DAYS_IN_MONTH[month];
    }

    public static int nextMonth(int month) {
        return month == DECEMBER.number() ? JANUARY.number() : month + 1;
    }

    private static boolean isLeap(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }
}
