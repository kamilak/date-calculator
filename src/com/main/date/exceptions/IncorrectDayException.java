package com.main.date.exceptions;

import com.main.date.Date;

/**
 * This exception is raised when requested to instantiate {@link Date}
 * with incorrect day number for given month and year.
 */
public class IncorrectDayException extends RuntimeException {
    public IncorrectDayException(int day, int endDayOfMonth) {
        super(String.format("Incorrect day %d (value not in range 1-%d).", day, endDayOfMonth));
    }
}
