package com.main.date;

import com.main.date.validations.DateValidator;

import static com.main.date.Month.DECEMBER;
import static com.main.date.utils.DateUtils.*;

/**
 * {@code Date} represents a calendar date, such as 2000-01-01.
 */
public class Date implements Comparable<Date> {

    private int year;
    private int month;
    private int day;

    public Date(int year, int month, int day) {
        DateValidator validator = new DateValidator(year, month, day);
        validator.validate();

        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Date nextDay() {
        if (day == getEndDayOfMonth(month, year)) {
            int newYear = month == DECEMBER.number() ? year + 1 : year;
            return new Date(newYear, nextMonth(month), 1);
        }
        return new Date(year, month, day + 1);
    }

    public int compareTo(Date otherDate) {
        if (this.isBefore(otherDate)) {
            return 1;
        } else if (this.isEqual(otherDate)) {
            return 0;
        } else {
            return -1;
        }
    }

    public boolean isEqual(Date otherDate) {
        return year == otherDate.year && month == otherDate.month && day == otherDate.day;
    }

    public boolean isBefore(Date otherDate) {
        if (year < otherDate.year) {
            return true;
        } else if (year == otherDate.year) {
            if (month < otherDate.month) {
                return true;
            } else if (month == otherDate.month) {
                return day < otherDate.day;
            }
        }
        return false;
    }

    public String toString() {
        return String.format("%d-%02d-%02d", year, month, day);
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
