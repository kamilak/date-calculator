package com.main;

import com.main.date.Date;
import com.main.date.DateParser;
import com.main.date.DaysCounter;
import com.main.exceptions.IncorrectParametersException;

/**
 * Main class that provides an entry point to the Date Calculator application.
 *
 * @author Kamila Kurys
 */
public class DateCalculator {

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new IncorrectParametersException();
            }

            DateParser dateParser = new DateParser();
            Date dateFrom = dateParser.parse(args[0]);
            Date dateTo = dateParser.parse(args[1]);

            System.out.println(String.format("%s – %s: %d days",
                    dateFrom, dateTo, new DaysCounter(dateFrom, dateTo).days()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
