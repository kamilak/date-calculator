package com.tests;

import com.main.date.Date;
import com.main.date.DaysCounter;
import org.junit.Test;

import java.time.LocalDate;

import static com.main.date.utils.DateUtils.*;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.assertEquals;

public class DaysCounterTest {

    @Test
    public void testCountWithSomeRandomDates() {
        Date dateFrom = new Date(1984, 7, 4);
        Date dateTo = new Date(1984, 12, 25);

        assertEquals(173, new DaysCounter(dateFrom, dateTo).days());
    }

    @Test
    public void testCountWithTwoSameDates() {
        Date dateFrom = new Date(2000, 1, 1);
        Date dateTo = new Date(2000, 1, 1);

        assertEquals(0, new DaysCounter(dateFrom, dateTo).days());
    }

    @Test
    public void testCountWithTwoConsecutiveDates() {
        Date dateFrom = new Date(2000, 1, 1);
        Date dateTo = new Date(2000, 1, 2);

        assertEquals(0, new DaysCounter(dateFrom, dateTo).days());
    }

    @Test
    public void testCountWhenDateFromIsAfterDateTo() {
        Date dateFrom = new Date(1989, 1, 3);
        Date dateTo = new Date(1983, 8, 3);

        assertEquals(1979, new DaysCounter(dateFrom, dateTo).days());
    }

    @Test
    public void testCountWithMinValidDateAndMaxValidDate() {
        LocalDate minDate = LocalDate.of(YEAR_MIN, 1, 1);
        LocalDate maxDate = LocalDate.of(YEAR_MAX,12,31);

        Date dateFrom = new Date(YEAR_MIN, 1, 1);
        Date dateTo = new Date(YEAR_MAX, 12, 31);

        assertEquals(DAYS.between(minDate, maxDate) - 1, new DaysCounter(dateFrom, dateTo).days());
    }
}
