package com.tests;

import com.main.date.Date;
import com.main.date.DateParser;
import com.main.date.exceptions.IncorrectDateFormatException;
import org.junit.Test;


import static org.junit.Assert.assertTrue;

public class DateParserTest {

    private DateParser dateParser = new DateParser();

    @Test
    public void testParseWithCorrectDateYYYYMMDD() {
        Date date = new Date(2000, 12, 12);
        assertTrue(date.isEqual(dateParser.parse("2000-12-12")));
    }

    @Test(expected = IncorrectDateFormatException.class)
    public void testParseWithIncorrectYearFormat() {
        dateParser.parse("16-12-12");
    }

    @Test(expected = IncorrectDateFormatException.class)
    public void testParseWithIncorrectMonthFormat() {
        dateParser.parse("2016-2-01");
    }

    @Test(expected = IncorrectDateFormatException.class)
    public void testParseWithIncorrectDayFormat() {
        dateParser.parse("2016-12-1");
    }
}
