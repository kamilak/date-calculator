# Date Calculator: Problem

You are tasked with calculating the number of full days elapsed in between start and end dates. 
The first and the last day are considered partial days and never counted. Following this logic, 
an experiment that has run from 1972-11-07 to 1972-11-08 should return 0, because there are no 
fully elapsed days contained in between those dates, and 2000-01-01 to 2000-01-03 should return 1. 
The solution needs to cater for all valid dates between 1901-01-01 and 2999-12-31.

Implement a command line based system with at least one way of providing input and output on the terminal. 
Although it should obvious, make sure the solution compiles and works. Although any dates specified 
within the valid date range listed above should work, here are a few test cases to validate the output of your program.

TEST CASES
1.  1983-06-02 – 1983-06-22: 19 days
2.  1984-07-04 – 1984-12-25: 173 days
3.  1989-01-03 – 1983-08-03: 1979 days

## Solution

``DateCalculator`` is a main client class responsible for invoking calculations on collected input dates. ``DateParser`` parses input date represented as string and creates proper date object. ``DaysCounter`` gives ability to count days between two given dates. ``Date`` represents calendar date and it allows to create valid date object and provides standard class operations. ``DateUtils`` contains helper methods required for ``DaysCounter`` for days calculation. Exceptions classes like ``IncorrectMonthException`` help to handle domain-specific exceptions.

## How to run

In project directory ``date_calculator`` 

Type ``java -jar out/artifacts/date_calculator_jar/date-calculator.jar`` then provide two input dates in format ``YYYY-MM-DD`` 
e.g. ``java -jar out/artifacts/date_calculator_jar/date-calculator.jar 1989-01-03 1983-08-03``
 
