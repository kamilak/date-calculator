package com.main.date;

import com.main.date.exceptions.IncorrectDateFormatException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

/**
 * This class converts from {@link String} date to {@link Date}.
 */
public class DateParser {

    private static final String regex = "^(\\d{4})-(\\d{2})-(\\d{2})$";

    /**
     * @param date The string to convert - must represent a valid calendar date
     * @throws IncorrectDateFormatException when input has incorrect format
     * @return converted {@link Date} object
     */
    public Date parse(String date) {
        Matcher matcher = Pattern.compile(regex).matcher(date);

        if (matcher.find()) {
            int year = parseInt(matcher.group(1));
            int month = parseInt(matcher.group(2));
            int day = parseInt(matcher.group(3));

            return new Date(year, month, day);
        }

        throw new IncorrectDateFormatException(date);
    }
}
