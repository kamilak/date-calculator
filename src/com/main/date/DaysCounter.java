package com.main.date;

import com.main.date.Date;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.Collections.max;
import static java.util.Collections.min;

/**
 * The class is responsible for calculating number of days between two given dates {@link Date}.
 *
 * Number of days will be the same regardless of given dates order.
 */
public class DaysCounter {

    private Date dateFrom;
    private Date dateTo;

    public DaysCounter(Date dateOne, Date dateTwo) {
        Comparator<Date> datesComparator = (o1, o2) -> dateOne.compareTo(dateTwo);
        List<Date> dates = Arrays.asList(dateOne, dateTwo);

        dateFrom = min(dates, datesComparator);
        dateTo = max(dates, datesComparator);
    }

    public int days() {
        int days = -1;
        do {
            days++;
            dateFrom = dateFrom.nextDay();
        } while (dateFrom.isBefore(dateTo));
        return days;
    }
}
