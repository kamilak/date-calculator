package com.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DateTest.class,
        DateParserTest.class,
        DaysCounterTest.class
})
public class JunitTestSuite {
}
