package com.tests;

import com.main.date.exceptions.IncorrectDayException;
import com.main.date.exceptions.IncorrectMonthException;
import com.main.date.exceptions.IncorrectYearException;
import com.main.date.Date;
import com.main.date.utils.DateUtils;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DateTest {

    @Test
    public void testNextDay() {
        LocalDate minDate = LocalDate.of(DateUtils.YEAR_MIN, 1, 1);
        LocalDate maxDate = LocalDate.of(DateUtils.YEAR_MAX,12,31);
        Date date = new Date(DateUtils.YEAR_MIN, 1, 1);

        while (minDate.isBefore(maxDate)) {
            minDate = minDate.plusDays(1);
            date = date.nextDay();
            assertEquals(minDate.toString(), date.toString());
        }
    }

    @Test
    public void testCreateDate() {
        Date date = new Date(2016, 5, 01);
        assertEquals(2016, date.getYear());
        assertEquals(05, date.getMonth());
        assertEquals(01, date.getDay());
    }

    @Test
    public void testToString() {
        assertEquals("2016-11-01", new Date(2016, 11, 1).toString());
        assertEquals("2016-05-11", new Date(2016, 5, 11).toString());
        assertEquals("2016-05-01", new Date(2016, 5, 1).toString());
    }

    @Test
    public void testIsEqual() {
        assertTrue(new Date(2016, 11, 1).isEqual(new Date(2016, 11, 1)));
    }

    @Test
    public void testIsEqualWithDifferentYear() {
        assertFalse(new Date(2016, 11, 1).isEqual(new Date(2015, 11, 1)));
    }

    @Test
    public void testIsEqualWithDifferentMonth() {
        assertFalse(new Date(2016, 11, 1).isEqual(new Date(2016, 12, 1)));
    }

    @Test
    public void testIsEqualWithDifferentDay() {
        assertFalse(new Date(2016, 11, 1).isEqual(new Date(2016, 11, 2)));
    }

    @Test
    public void testIsBeforeWhenDateFromEqualDateTo() {
        assertFalse(new Date(2016, 11, 1).isBefore(new Date(2016, 11, 1)));
    }

    @Test
    public void testIsBeforeWhenDateFromBeforeDateTo() {
        assertTrue(new Date(2016, 11, 1).isBefore(new Date(2016, 11, 2)));
    }

    @Test
    public void testIsBeforeWhenDateFromAfterDateTo() {
        assertFalse(new Date(2016, 11, 3).isBefore(new Date(2016, 11, 2)));
    }

    @Test
    public void testCompareTo(){
        assertEquals(1, new Date(2016, 11, 1).compareTo(new Date(2016, 11, 2)));
        assertEquals(0, new Date(2016, 11, 1).compareTo(new Date(2016, 11, 1)));
        assertEquals(-1, new Date(2016, 11, 2).compareTo(new Date(2016, 11, 1)));
    }

    @Test(expected = IncorrectYearException.class)
    public void testDateWithYearBeforeMinValidYear() {
        new Date(DateUtils.YEAR_MIN - 1, 1, 1);
    }

    @Test(expected = IncorrectYearException.class)
    public void testDateWithYearAfterMaxValidYear() {
        new Date(DateUtils.YEAR_MAX + 1, 1, 1);
    }

    @Test(expected = IncorrectMonthException.class)
    public void testDateWithInvalidMonth() {
        new Date(2016, 13, 1);
    }

    @Test(expected = IncorrectDayException.class)
    public void testDateWithInvalidDay() {
        new Date(1901, 1, -1);
    }

    @Test(expected = IncorrectDayException.class)
    public void testDateWithInvalidDayForMonth() {
        new Date(1901, 2, 30);
    }

    @Test(expected = IncorrectDayException.class)
    public void testDateWith29FebruaryForNoLeapYear() {
        new Date(2015, 2, 29);
    }
}
